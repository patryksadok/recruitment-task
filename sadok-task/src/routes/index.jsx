import React from "react";
import { Route } from "react-router-dom";
import About from '../pages/about';
import SkiCams from '../pages/skicams';
import Contact from '../pages/contact';

const AppRouter = () => {
  return (
      <>
        <Route path="/about" exact={true} component={About} />
        <Route path="/ski-cams" exact={true} component={SkiCams} />
        <Route path="/contact" exact={true} component={Contact} />
      </>
  );
}

export default AppRouter;