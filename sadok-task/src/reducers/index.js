import { combineReducers } from "redux";

import skicamReducer from "./skicamReducer";

export default combineReducers({
    skicamReducer,
});
