//import {} from "../config/actionTypes";
import {
    BEGIN_GET_SKICAMS,
    SUCCESS_GET_SKICAMS,
    FAILURE_GET_SKICAMS,
    CLEAR_SKICAMS
} from '../actionConfig/skicam';

const initialState = {
    skicams: [],
    loading: false,
    error: null
};

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case CLEAR_SKICAMS:
            return {
                ...state,
                skicams: [],
                error: null,
                loading: false
            };
        case BEGIN_GET_SKICAMS:
            return {
                ...state,
                loading: true
            };
        case SUCCESS_GET_SKICAMS:
            return {
                ...state,
                loading: false,
                skicams: action.payload
            };
        case FAILURE_GET_SKICAMS:
            return {
                ...state,
                loading: false,
                error: action.payload
            };
        default:
            return { ...state };
    }
};
