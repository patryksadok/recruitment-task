import axios from "../utils/axiosProvider";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { parseSkicams } from '../utils/camsParser';

import {
    BEGIN_GET_SKICAMS,
    SUCCESS_GET_SKICAMS,
    FAILURE_GET_SKICAMS,
    CLEAR_SKICAMS
} from '../actionConfig/skicam';

const namesToParse = ['Andalo', 'Monte Bondone'];

const getSkicamsBegin = () => ({
    type: BEGIN_GET_SKICAMS
});

const getSkicamsSuccess = (skicams) => ({
    payload: skicams,
    type: SUCCESS_GET_SKICAMS
});

const getSkicamsFailure = (error) => ({
    payload: error,
    type: FAILURE_GET_SKICAMS
});

export const getSkicams = () => {
    return (dispatch) => {
        dispatch(getSkicamsBegin());
        return axios
            .get('https://makevoid-skicams.p.mashape.com/cams.json')
            .then((res) => {
                const skicams = res;
                const parsedCams = parseSkicams(skicams.data, namesToParse);
                dispatch(getSkicamsSuccess(parsedCams));
                toast.success("Successfully downloaded data");
            })
            .catch((error) => {
                dispatch(getSkicamsFailure(error));
                toast.error("An error occured");
            });
    };
};

const clearSkicamsAction = () => ({
    type: CLEAR_SKICAMS
});

export const clearSkicams = () => {
    return (dispatch) => {
        dispatch(clearSkicamsAction());
    };
};

