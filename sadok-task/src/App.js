import React, { Component } from 'react';
import Site from './layout/index';
import { ThemeProvider } from 'styled-components';
import theme from './styled/theme';
import GlobalStyles from './styled/Global';

class App extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <>
          <GlobalStyles />
            <Site/>
        </>
      </ThemeProvider>
    );
  }
}

export default App;