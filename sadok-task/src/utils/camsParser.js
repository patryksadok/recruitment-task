export const parseSkicams = (camsObject, names) => {
    const parsedArray = [];
    const camsArray = Object.values(camsObject);
    
    camsArray.forEach(element => {
        names.forEach(parsingName => {
            if (element.name === parsingName) {
                const cams = Object.values(element.cams);
                const cam = {name: element.name, cams: cams};
                parsedArray.push(cam);
            }
        })
    });
    
    return parsedArray;
}