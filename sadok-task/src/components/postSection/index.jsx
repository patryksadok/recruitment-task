import React from 'react';
import {
    SectionContainer
} from './PostSection.styled';
import Section from './section';

const section = {
    header: 'Lorem Ipsum',
    value: 'Etiam ullamcorper. Suspendisse a pellentesque dui, non felis maecenas.'
};

const Index = () => {
    return (
        <SectionContainer>
            <Section header={section.header} text={section.value} />
            <Section header={section.header} text={section.value} />
            <Section header={section.header} text={section.value} />
        </SectionContainer>
    );
};

export default Index;
