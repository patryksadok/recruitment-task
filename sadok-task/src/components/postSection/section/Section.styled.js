import styled from 'styled-components';

const SectionWrapper = styled('div')`
    width: 100%;
    margin-bottom: 30px;
    @media screen and (min-width: ${props =>props.theme.breakpoints.tablet}) {
        box-shadow: 1px 1px 1px ${props =>props.theme.colors.darkGray};
        width: 30%;
    }
`;

const Section = styled('div')`
    background-color: ${props => props.theme.colors.white};
    box-shadow: 1px 1px 1px grey;
    width: 100%;
`;

const PhotoContainer = styled('div')`
    background-image: url('https://cdn.pixabay.com/photo/2019/03/25/22/47/bmx-4081517_960_720.png');
    position: relative;
    height: ${props => props.theme.postSection.photoHeight};
    background-position: center;
    display: flex;
`;

const ContentContainer = styled('div')`
    background-color: ${props => props.theme.colors.white};
    padding-left: ${props => props.theme.postSection.sidePadding};
    padding-right: ${props => props.theme.postSection.sidePadding};
    padding-top: ${props => props.theme.postSection.topPadding};
    padding-bottom: ${props => props.theme.postSection.topPadding};
`;

const ContentHeader = styled('h2')`
    margin-top: ${props => props.theme.postSection.headerMargin};
    margin-bottom: ${props => props.theme.postSection.headerMargin};
`;

const ContentValue = styled('div')`
    margin-top: ${props => props.theme.postSection.contentMargin};
`

export {
    Section,
    PhotoContainer,
    ContentContainer,
    ContentHeader,
    ContentValue,
    SectionWrapper
}
