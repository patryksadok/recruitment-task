import React from 'react';
import PropTypes from 'prop-types';
import {
    Section,
    PhotoContainer,
    ContentContainer,
    ContentHeader,
    ContentValue,
    SectionWrapper
} from './Section.styled';

const Index = (props) => {
    return (
        <>
            <SectionWrapper>
                <Section>
                    <PhotoContainer/>
                    <ContentContainer>
                        <ContentHeader>
                            {props.header}
                        </ContentHeader>
                        <ContentValue>
                            {props.text}
                        </ContentValue>
                    </ContentContainer>
                </Section>
            </SectionWrapper>
        </>
    );
};

Index.propTypes = { 
    header: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired
};

Index.defaultProps = {
    header: 'Default header',
    text: 'Default text'
};

export default Index;
