import styled from 'styled-components';

const SectionContainer = styled('div')`
    width: 100%;
    height: 100%;
    display: flex;
    flex-direction: column;
    position: relative;
    justify-content: space-between;
    margin-bottom: ${props =>props.theme.paddings.contentBottomPadding};

    @media screen and (min-width: ${props =>props.theme.breakpoints.tablet}) {
        flex-direction: row;
        margin-bottom: 0;
    }
`;

export {
    SectionContainer
}
