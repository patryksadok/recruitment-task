import styled from 'styled-components';

const SkicamWrapper = styled('div')`
    width: 100%;
    text-align: center;
    display: flex;
    flex-direction: column;
    position:relative;
    justify-content: space-between;
    margin-bottom: ${props =>props.theme.skicamSection.wrapperMargin};
    
    @media screen and (min-width: ${props =>props.theme.breakpoints.tablet}) {
        width: 45%;
    }
`;

const HeaderWrapper = styled('div')`
    background-color: ${props =>props.theme.colors.white};
    height: ${props =>props.theme.skicamSection.headerWrapperHeight};
    text-align: center;
    position: relative;
    display: flex;
    align-items: center;
    justify-content: space-between;
`;

const DateWrapper = styled('div')`
    position: absolute;
    top: ${props =>props.theme.skicamSection.dateTopMargin};
    right: ${props =>props.theme.skicamSection.dateSideMargin};
    font-size: ${props =>props.theme.skicamSection.dateFontSize};
    color: ${props =>props.theme.colors.lightGray};
`;

const Header = styled('div')`
    margin-left: ${props =>props.theme.skicamSection.headerMargin};
    margin-right: ${props =>props.theme.skicamSection.headerMargin};
    width: 100%;
`;

const HeaderName = styled('div')`
    text-align: center;
`;

const Image = styled('img')`
    width: 100%;
    height: 100%;
`;

export {
    SkicamWrapper,
    HeaderWrapper,
    DateWrapper,
    Header,
    HeaderName,
    Image
};
