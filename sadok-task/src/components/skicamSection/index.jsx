import React from 'react';
import moment from 'moment';
import {
    SkicamWrapper,
    HeaderWrapper,
    DateWrapper,
    Header,
    HeaderName,
    Image
} from './Skicam.styled';

const Index = (props) => {
    const cam = props.cam;
    const cams = cam.cams;
    const camsArray = Object.values(cams);
    const dateNow = moment().format('DD-MM-YYYY');

    const getRandomImage = () => {
        const index = camsArray[Math.floor(Math.random()*camsArray.length)];
        return index.url;
    };

    return (
        <SkicamWrapper>  
            <HeaderWrapper>
                <Header>
                    <HeaderName>
                        <h3>{cam.name}</h3>
                    </HeaderName>
                    <DateWrapper>
                        {dateNow}
                    </DateWrapper>
                </Header>
            </HeaderWrapper>
            <Image src={getRandomImage()}/>
            <Image src={getRandomImage()}/>
        </SkicamWrapper>
    );
};

export default Index;
