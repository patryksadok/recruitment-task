import React from 'react';
import {
    FormContainer,
    StyledInput,
    StyledButton,
    UpperContainer,
    BottomContainer,
    StyledForm,
    ValidationLabel,
    InputContainer,
    Placeholder,
    PlaceholderMark
} from './ContactForm.styled';
import handleForm from './handleForm';
import { validate } from '../../validators/contactForm';

const Index = (props) => {
    const {
        values,
        errors,
        handleChange,
        handleSubmit,
      } = handleForm(validate);

    return (
        <>
            <FormContainer>
                <StyledForm noValidate>
                    <UpperContainer>
                        <InputContainer>
                            <StyledInput type="text" name="name" onChange={handleChange} value={values.name || ''}/>
                            {!values.name && (
                                <Placeholder>
                                    Name<PlaceholderMark> *</PlaceholderMark>
                                </Placeholder>
                            )}
                        </InputContainer>
                        {errors.name && (
                            <ValidationLabel>{errors.name}</ValidationLabel>
                        )}
                        <InputContainer>
                            <StyledInput type="email" name="email" onChange={handleChange} value={values.email || ''}/>
                            {!values.email && (
                                <Placeholder>
                                    Email<PlaceholderMark> *</PlaceholderMark>
                                </Placeholder>
                            )}
                        </InputContainer>
                        {errors.email && (
                            <ValidationLabel>{errors.email}</ValidationLabel>
                        )}
                    </UpperContainer>
                    <BottomContainer>
                        <StyledInput type="text" name="email" placeholder="Message"/>
                        <StyledButton onClick={handleSubmit}>SEND</StyledButton>
                    </BottomContainer>
                </StyledForm>
            </FormContainer>
        </>
    );
};

export default Index;
