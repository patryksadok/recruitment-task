    
import { useState, useEffect } from 'react';

const HandleForm = (validate) => {

  const [values, setValues] = useState({});
  const [errors, setErrors] = useState({});

  useEffect(() => {}, [errors]);

  const handleSubmit = (event) => {
    if (event) event.preventDefault();
    setErrors(validate(values));
  };

  const handleChange = (event) => {
    event.persist();
    setValues(values => ({ ...values, [event.target.name]: event.target.value }));
  };

  return {
    handleChange,
    handleSubmit,
    values,
    errors,
  }
};

export default HandleForm;
