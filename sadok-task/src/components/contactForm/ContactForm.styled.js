import styled from 'styled-components';

const FormContainer = styled('div')`
    background-color: ${props =>props.theme.colors.white};
    padding-top: ${props =>props.theme.contactForm.topPadding};
    padding-bottom: ${props =>props.theme.contactForm.mobileBottomPadding};
    padding-left: ${props =>props.theme.contactForm.sidePadding};
    padding-right: ${props =>props.theme.contactForm.sidePadding};
    display: flex;
    flex-direction: column;
    width: 90%;
    justify-content: center;
    @media screen and (min-width: ${props =>props.theme.breakpoints.tablet}) {
        padding-bottom: ${props =>props.theme.contactForm.tabletBottomPadding};
        width:70%;
    }
    @media screen and (min-width: ${props =>props.theme.breakpoints.desktop}) {
        width:40%;
        padding-bottom: ${props =>props.theme.contactForm.desktopBottomPadding};
    }
`;

const StyledForm = styled('form')`
    width: 80%;
    margin: 0 auto;
`;

const StyledInput = styled('input')`
    margin-top: ${props =>props.theme.contactForm.inputMargin};
    margin-bottom: ${props =>props.theme.contactForm.inputMargin};
    border: none;
    border-bottom: 1px solid ${props =>props.theme.colors.lightGray};
    padding-top: ${props =>props.theme.contactForm.inputPadding};
    padding-bottom: ${props =>props.theme.contactForm.inputPadding};
    color: ${props =>props.theme.colors.lightGray};
    font-size: ${props =>props.theme.contactForm.inputFontSize};
    width: 100%;
    font-family: ${props =>props.theme.contactForm.font};
`;

const StyledButton = styled('button')`
    background-color: ${props =>props.theme.colors.skyBlue};
    color: ${props =>props.theme.colors.white};
    bottom: 0;
    right: 0;
    border: 0;
    padding-top: ${props =>props.theme.contactForm.buttonTopPadding};
    padding-bottom: ${props =>props.theme.contactForm.buttonTopPadding};
    padding-left: ${props =>props.theme.contactForm.buttonSidePadding};
    padding-right: ${props =>props.theme.contactForm.buttonSidePadding};
    width: 100%;
    margin-top: ${props =>props.theme.contactForm.buttonMargin};
    font-family: ${props =>props.theme.contactForm.font};
    @media screen and (min-width: ${props =>props.theme.breakpoints.tablet}) {
        align-self: flex-end;
        width: 30%;
    }
`;

const UpperContainer = styled('div')`
    display: flex;
    flex-direction: column;
`;

const BottomContainer = styled('div')`
    position: relative;
    margin-top: ${props =>props.theme.contactForm.bottomContainerMargin};
    display: flex;
    flex-direction: column;
`;

const ValidationLabel = styled('p')`
    color: ${props =>props.theme.colors.red};
`;

const InputContainer = styled('div')`
    position: relative;
`;

const Placeholder = styled('div')`
    position: absolute;
    pointer-events: none;
    top: 14px;
    bottom: 2px;
    left: 0px;
    margin: auto;
    color: ${props =>props.theme.colors.lightGray};
    font-size: ${props =>props.theme.contactForm.font};
`;

const PlaceholderMark = styled('span')`
    color: ${props =>props.theme.colors.red};
`;

export {
    FormContainer,
    StyledInput,
    StyledButton,
    UpperContainer,
    BottomContainer,
    StyledForm,
    ValidationLabel,
    InputContainer,
    Placeholder,
    PlaceholderMark
};
