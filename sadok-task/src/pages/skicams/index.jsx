import {connect} from 'react-redux';
import Skicams from './skicams';
import {
    getSkicams,
    clearSkicams
} from '../../actions/skicamActions';

const mapStateToProps = (state) => {
    return {
        skicams: state.skicamReducer.skicams
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getSkicams: () => dispatch(getSkicams()),
        clearSkicams: () => dispatch(clearSkicams())
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Skicams);
