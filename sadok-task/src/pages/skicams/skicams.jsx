import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import Skicam from '../../components/skicamSection';
import {
    Container
} from './skicams.styled';

const Index = (props) => {

    useEffect(() => {
        props.getSkicams();
        return () => props.clearSkicams();
    }, []);
    
    if (props.skicams.length > 0) {
        const skicams = props.skicams;
        return (
            <Container>
                <Skicam cam={skicams[0]}/>
                <Skicam cam={skicams[1]}/>
            </Container>
        );
    }

    return (
        <div>Fetching data from URL...</div>
    )
};

Index.propTypes = {
    skicams: PropTypes.array.isRequired
};

Index.defaultProps = {
    skicams: []
};

export default Index;
