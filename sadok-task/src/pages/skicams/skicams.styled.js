import styled from 'styled-components';

const Container = styled('div')`
    position: relative;
    display: flex;
    flex-direction: column;
    margin-bottom: 25px;
    flex-wrap: wrap;
    width: 100%;
    justify-content: space-between;
    margin-bottom: ${props =>props.theme.paddings.contentBottomPadding};

    @media screen and (min-width: ${props =>props.theme.breakpoints.tablet}) {
        flex-direction: row;
        margin-bottom: 0;
    }
`;

export {
    Container
}
