export const validate = (values) => {
    let errors = {};
    if (!values.email || /\s/.test(values.email)) {
        errors.email = 'Email address is required';
    } else if (!/\S+@\S+\.\S+/.test(values.email) || /\s/.test(values.email)) {
        errors.email = 'Email address has invalid format';
    }
    if (!values.name || /\s/.test(values.name)) {
        errors.name = 'Name is required';
    }

    return errors;
};
