
import React from 'react';
import {
    FooterContainer,
    FooterContent
} from './Footer.styled';

const Index = () => {
    return (
        <FooterContainer>
            <FooterContent>
                Powered by PGS
            </FooterContent>
        </FooterContainer>
    );
};

export default Index;