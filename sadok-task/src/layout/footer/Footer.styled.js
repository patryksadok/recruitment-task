import styled from 'styled-components';

const FooterContainer = styled('div')`
    background-color: ${props =>props.theme.colors.darkGray};
    height: ${props =>props.theme.footer.height};
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    position: absolute;
    bottom: 0;
    @media screen and (min-width: ${props =>props.theme.breakpoints.tablet}) {
    }
`;
const FooterContent = styled('div')`
    border-top: 1px solid ${props =>props.theme.colors.lightGray};
    padding-top: ${props =>props.theme.footer.paddingVertical};
    padding-bottom: ${props =>props.theme.footer.paddingVertical};
    padding-left: ${props =>props.theme.footer.paddingHorizontal};
    padding-right: ${props =>props.theme.footer.paddingHorizontal};
    color: ${props =>props.theme.colors.lightGray};

`;
export {
    FooterContainer,
    FooterContent
}