
import React from 'react';
import Header from './header';
import Footer from './footer';
import Router from '../routes/index';

const Index = () => {
    return (
        <>
            <div className="site-wrapper">
                <Header/>
                    <div className="content-wrapper">
                    <Router/>
                    </div>
            </div>
            <Footer/>
        </>
    );
};

export default Index;
