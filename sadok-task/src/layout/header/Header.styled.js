import styled from 'styled-components';
import { NavLink } from 'react-router-dom';

const StyledLink = styled(NavLink)`
    &:link, &:visited  {
        text-decoration: none;
        color: ${props =>props.theme.colors.black};
    }
    &:hover, &:focus, &:active {
        border-bottom: 2px solid ${props =>props.theme.colors.orange};
        color: ${props =>props.theme.colors.orange};
    }

    @media screen and (min-width: ${props =>props.theme.breakpoints.tablet}) {
        padding-bottom: ${props =>props.theme.header.linkPadding};
    }
`;

const Logo = styled('img')`
    width: 100px;
    height: 50px;
    margin-left: 8%;
    margin-top: 20px;
    margin-bottom: 20px;
    @media screen and (min-width: ${props =>props.theme.breakpoints.tablet}) {
        margin-left: ${props =>props.theme.margins.tablet};
        margin-right: ${props =>props.theme.margins.tablet};
        flex-direction: row;
        align-items: flex-end;
    }
    @media screen and (min-width: ${props =>props.theme.breakpoints.desktop}) {
        margin-left: ${props =>props.theme.margins.desktop};
        margin-right: ${props =>props.theme.margins.desktop};
        flex-direction: row;
        align-items: flex-end;
    }
`;

const LinkContainer = styled('div')`
    @media screen and (min-width: ${props =>props.theme.breakpoints.tablet}) {
        margin-right: 5%;
        margin-top: 0%;
        margin-bottom: 0;
    }
    margin-top: 1%;
    margin-bottom: 1%;
`;

const MenuContainer = styled('div')`
    display: block;
    background-color: ${props =>props.theme.colors.white};
    border-bottom: 1px solid ${props =>props.theme.colors.lightGray};
    .active {
        border-bottom: 2px solid ${props =>props.theme.colors.orange};
    }
    @media screen and (min-width: ${props =>props.theme.breakpoints.tablet}) {
        padding-bottom: ${props =>props.theme.header.padding};
    }
`;

const LinkWrapper = styled('div')`
    margin-left: ${props =>props.theme.margins.mobile};
    margin-right: ${props => props.theme.margins.mobile};
    display: flex;
    flex-direction: column;
    align-items: center;

    @media screen and (min-width: ${props =>props.theme.breakpoints.tablet}) {
        margin-left: ${props =>props.theme.margins.tablet};
        margin-right: ${props =>props.theme.margins.tablet};
        flex-direction: row;
        align-items: flex-end;
    }
    @media screen and (min-width: ${props =>props.theme.breakpoints.desktop}) {
        margin-left: ${props =>props.theme.margins.desktop};
        margin-right: ${props =>props.theme.margins.desktop};
        flex-direction: row;
        align-items: flex-end;
    }
`;

export {
    LinkContainer,
    LinkWrapper,
    MenuContainer,
    StyledLink,
    Logo
}