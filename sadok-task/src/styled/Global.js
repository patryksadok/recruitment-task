import {
    createGlobalStyle,
    css,
} from 'styled-components';
  
const globalCss = css`
    html {
        min-height: 100%;
        position: relative;
        margin: 0px;
        padding: 0px
    }

    #root {
        min-height: 100%;
        overflow: hidden
    }

    body {
        @import url('https://fonts.googleapis.com/css?family=Roboto');
        font-family: 'Roboto', sans-serif !important;
        background-color: ${props =>props.theme.colors.delicateGray};
        margin: 0px;
        padding: 0px;
        width: 100%;
        height: 100%;
    }
    
    .site-wrapper {
        position: relative;
        height: 100% !important;
        min-height: 100% !important;
    }
    
    .content-wrapper {
        display: flex;
        justify-content: center;
        height: 100%;
        min-height: 100%;
        margin-left: ${props =>props.theme.margins.mobile};
        margin-right: ${props => props.theme.margins.mobile};
        display: flex;
        flex-direction: column;
        align-items: center;
        padding-top: ${props =>props.theme.paddings.mobilePadding};
        padding-bottom: ${props =>props.theme.paddings.mobilePadding};
        
        @media screen and (min-width: ${props =>props.theme.breakpoints.tablet}) {
            margin-left: ${props =>props.theme.margins.tablet};
            margin-right: ${props =>props.theme.margins.tablet};
            flex-direction: row;
            align-items: flex-end;
            height: 100%;
            padding-top: ${props =>props.theme.paddings.tabletPadding};
            padding-bottom: ${props =>props.theme.paddings.tabletPadding};
        }
        @media screen and (min-width: ${props =>props.theme.breakpoints.desktop}) {
            margin-left: ${props =>props.theme.margins.desktop};
            margin-right: ${props =>props.theme.margins.desktop};
            flex-direction: row;
            align-items: flex-end;
            padding-top: ${props =>props.theme.paddings.contentPadding};
            padding-bottom: ${props =>props.theme.paddings.contentBottomPadding};
        }
    }
`;
  
export default createGlobalStyle`
    ${globalCss}
`;