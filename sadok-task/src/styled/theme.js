// Shared variables
const fontSizes = {
    xsmall: '0.6rem',
    small: '0.75rem',
    medium: '1rem',
    big: '1.25rem',
    large: '1.5rem',
    xlarge: '1.75rem',
};

const colors = {
    orange: '#c9830a',
    black: '#000000',
    white: '#ffffff',
    darkGray: '#2d2d2d',
    mediumGray: '#8e8e8e',
    lightGray: '#a8a6a6',
    delicateGray: '#dee0e2',
    red: '#FF0000',
    skyBlue: '#00BFFF'
};

const margins = {
    desktop: '15%',
    tablet: '10%',
    mobile: '5%'
};

const paddings = {
    contentPadding: '100px',
    contentBottomPadding: '200px',
    tabletPadding: '50px',
    mobilePadding: '25px'
}

const breakpoints = {
    tablet: '768px',
    desktop: '1200px'
};

// Header variables
const header = {
    height: '40px',
    linkPadding: '9px',
    padding: '10px'
};

// Footer variables
const footer = {
    height: '100px',
    paddingVertical: '15px',
    paddingHorizontal: '80px'
};

// About posts variables
const postSection = {
    marginBottom: '30px',
    photoHeight: '250px',
    sidePadding: '20px',
    topPadding: '30px',
    contentMargin: '10px',
    headerMargin: '0px'
};

// Skicams posts variables
const skicamSection = {
    wrapperMargin: '50px',
    headerWrapperHeight: '60px',
    dateTopMargin: '25%',
    dateSideMargin: '5%',
    dateFontSize: '12px',
    headerMargin: '15px'
};

// Form variables
const contactForm = {
    topPadding: '80px',
    mobileBottomPadding: '140px',
    tabletBottomPadding: '160px',
    desktopBottomPadding: '110px',
    sidePadding: '40px',
    inputMargin: '10px',
    inputPadding: '5px',
    inputFontSize: '14px',
    font: "'Roboto', sans-serif !important",
    buttonTopPadding: '10px',
    buttonSidePadding: '20px',
    buttonMargin: '25px',
    bottomContainerMargin: '100px',

}
export default {
    fontSizes,
    colors,
    margins,
    breakpoints,
    header,
    footer,
    postSection,
    paddings,
    skicamSection,
    contactForm
}